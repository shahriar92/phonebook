-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2016 at 08:07 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `miniproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE IF NOT EXISTS `phonebooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL,
  `telephone` int(11) NOT NULL,
  `description` text NOT NULL,
  `detail` text NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `phonebooks`
--

INSERT INTO `phonebooks` (`id`, `title`, `operator`, `mobile`, `telephone`, `description`, `detail`, `deleted_at`) VALUES
(8, 'shahriar', 'Grameenphone', 1778142479, 147852, '<p><strong>i am shahriar.</strong></p>', 'i am shahriar.', NULL),
(11, 'Monirul islam', 'Robi', 1838404139, 14852, '<p><strong>I am monirul</strong></p>', 'I am monirul', NULL),
(13, 'abdul halim', 'citycel', 1191458725, 1265478, '<p><em><strong>i am abdul kalam</strong></em></p>', 'i am abdul kalam', NULL),
(14, 'Md.Mofizul Islam', 'Grameenphone', 1719564039, 1244555, '<p>&nbsp;I am mofiz</p>', '&nbsp;I am mofiz', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
