<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Miniproject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
   use App\BITM\SEIP107897\Phonebook\Phonebook;
use  App\BITM\SEIP107897\Utility\Utility;

$book = new Phonebook();
    $books =$book->getAllTitle(); 
    
    $filter=array();
    $filterOperator = isset($_POST['filterOperator'])?$_POST['filterOperator']:"";
    $filterTitle  = isset($_POST['filterTitle'])?$_POST['filterTitle']:"";
    
    $search = "";
    
    
    //Utility::dd($search);
    if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
          $filter = $_POST;
          $books = $book->index($filter);
    }
    
    if( strtoupper($_SERVER['REQUEST_METHOD']) == 'GET'){
          $search = isset($_GET['search'])?$_GET:array('search'=>'');
          $books = $book->index($search);
    }
    
    
   

 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Phone Book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
          <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    </head>
    <body>
        <h1>phone Book</h1>
        
        <div id="message">
            <?php echo Utility::message(); ?>            
        </div>
        
        <div>
        <form action="index.php" method="post" >
            <div>
                <label>Filter By Operator:</label>
                <input type="text" name="filterOperator" value="<?php echo $filterOperator;?>" /> 
                
                <label>Filter By Title:</label>
                <input type="text" name="filterTitle" id="filterTitle" value="<?php echo $filterTitle;?>" /> 
                
                <button type="submit"> GO </button>
            </div>
        </form>
            
        <form action="index.php" method="get" >
            <div>
                <label>Search</label>
                <input type="text" name="search" value="<?php echo $search['search'];?>" /> 
                
                <button type="submit"> Search </button>
            </div>
        </form>
            
            <span id="utility">Download as <a href="pdf.php" target="_blank">PDF</a> | <a href="excel.php">XL</a> |<a href="create.php"> Add New </a>
            | <a href="trashed.php"> All Trashed Books</a> |
            </span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>ID</th>
                    <th>Title &dArr;</th>
                     <th>Operator &dArr;</th>
                     <th>Mobile &dArr;</th>
                     <th>Telephone &dArr;</th>
                     <th>Description&dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               if(count($books) > 0){
               foreach($books as $book){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                   
                    <td><?php echo $book['id'];?></td>
                    
                    <td><a href="show.php?id=<?php echo $book['id'];?>"><?php echo $book['title'];?></a></td>
                    
                    <td><?php echo $book['operator'];?></td>
                    
                    <td><?php echo $book['mobile'];?></td>
                    
                    <td><?php echo $book['telephone'];?></td>
                    
                    <td><?php echo $book['description'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $book['id'];?>">View</a>
                        | <a href="edit.php?id=<?php echo $book['id'];?>">Edit</a> 
                        
                        | <a href="delete.php?id=<?php echo $book['id'];?>" class="delete">Delete</a>
                       
                        | <a href="trash.php?id=<?php echo $book['id'];?>">Trash</a>
                </tr>
            <?php
           $slno++;
            }
               }else{
                   ?>
                <tr><td colspan="5"> No record found</td></tr>
                <?php
               }
            ?>
            </tbody>
        </table>
        
        <div><span> prev  1 | 2 | 3 next </span></div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
          <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
          <script>
  $(function() {
    var availableTags = [
         '<?php echo implode("','",$titles);?>'   
    ];
    $( "#filterTitle" ).autocomplete({
      source: availableTags
    });
  });
  </script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(5000);
    
        </script>
    </body>
</html>
