<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."Miniproject".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

use App\BITM\SEIP107897\Phonebook\Phonebook;
use  App\BITM\SEIP107897\Utility\Utility;

$book = new Phonebook();
$books = $book->index();
$trs = "";

?>


                
                <?php
                $slno =0;
                foreach($books as $book):
                    $slno++;
                    $trs .="<tr>";
                    $trs .="<td>".$slno."</td>";
                    $trs .="<td>".$book['title']."</td>";
                    $trs .="<td>".$book['operator']."</td>";
                    $trs .="<td>".$book['mobile']."</td>";
                    $trs .="<td>".$book['telephone']."</td>";
                    $trs .="<td>".$book['description']."</td>";
                   // $trs .="<td><img src=../Resources/img/".$book['coverpage']." height='50' width='50' /></td>";
                    $trs .="</tr>";
                 endforeach;   
                ?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List of phone numbers</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1>List Of phone Numbers</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    
                    <th>Name &dArr;</th>
                    <th>Operator &dArr;</th>
                    <th>Mobile &dArr;</th>
                    <th>Telephone &dArr;</th>
                    <th>Description &dArr;</th>
                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>
       
        
            

    </body>
</html>
BITM;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."Miniproject".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

