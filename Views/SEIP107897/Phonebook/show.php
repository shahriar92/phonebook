<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Miniproject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use App\BITM\SEIP107897\Phonebook\Phonebook;
use  App\BITM\SEIP107897\Utility\Utility;

$obj = new Phonebook();
$book = $obj->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Phone Book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
    <body>
<h1>phone Number detail</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $book['id']; ?></dd>
    
    <dt>Title</dt>
    <dd><?php echo $book['title']; ?></dd>
    
    <dt>Operator</dt>
    <dd><?php echo $book['operator']; ?></dd>
    
    <dt>Mobile</dt>
    <dd><?php echo $book['mobile']; ?></dd>
    
    <dt>Telephone</dt>
    <dd><?php echo $book['telephone']; ?></dd>
 
    <dt>Description</dt>
    <dd><?php echo $book['description']; ?></dd>
</dl>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>

    </body>
</html>