<?php
namespace App\BITM\SEIP107897\Phonebook;

use \App\BITM\SEIP107897\Utility\Utility;
use App\BITM\SEIP107897\Model\Model;

class Phonebook extends Model{
    
    public $table = "phonebooks";
    
    public $id = "";
    public $title = "";
    public $operator="";
    public $mobile="";
    public $telephone="";
//  public $author = "";
    public $description = "";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    public $deleted_at = null; //soft delete
    
     public function __construct($data = false){
         
       parent::__construct();
       
       $this->id =$data['id'];// you pass data construct but you dont riceve this data 
       $this->title =$data['title'];
       $this->operator =$data['operator'];
       $this->mobile =$data['mobile'];
       $this->telephone =$data['telephone'];
       $this->description =$data['description'];
      
   
     }
    
    
    public function store($data = array()){
       
        $data = array('phonebooks'=>$data);
        
        $data['phonebooks']['detail'] = strip_tags($data['phonebooks']['description']);
        
        if($this->insert($data)){
            Utility::message("Number is added successfully.");           
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    public function show($id=false){
 
        $query = "SELECT * FROM `phonebooks` WHERE id =".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    
    public function getAllTitle(){
        $titles = array();

        $query = "SELECT title FROM `phonebooks` WHERE deleted_at IS NULL ";
        
        
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $titles[] = $row['title'];
        }
        return $titles;
    }
    
    public function index( $data=array() ){
        
        $whereClause = "AND 1=1";
        if(array_key_exists('search',$data)){
            $whereClause .= " AND title LIKE '%".$data['search']."%'";
            $whereClause .= " OR operator LIKE '%".$data['search']."%'";
            $whereClause .= " OR detail LIKE '%".$data['search']."%'";
        }else{
            $filter = $data;
            if(is_array($filter) && count($filter) > 0){

              if(array_key_exists('filterTitle', $filter) && !empty($filter['filterTitle']) ){
                  $whereClause .= " AND title LIKE '%".$filter['filterTitle']."%'";
              }

              if(array_key_exists('filterOperator', $filter)  && !empty($filter['filterOperator']) ){
                  $whereClause .= " AND operator LIKE '%".$filter['filterOperator']."%'";
              }

            }
        }
        
        $books = array();

        $query = "SELECT * FROM `phonebooks` WHERE deleted_at IS NULL " .$whereClause;
        //Utility::d($query);
        
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $books[] = $row;
        }
        return $books;
    }
    
    
    public function trashed(){
        
        $books = array();
        

        $query = "SELECT * FROM `phonebooks` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $books[] = $row;
        }
        return $books;
    }
    
    
 
    
    
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        


        $query = "DELETE FROM `miniproject`.`phonebooks` WHERE `phonebooks`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Information is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
        public function deletemultiple($ids = array()){
       
            if(is_array($ids) && count($ids) > 0){

                 //Utility::dd($ids);
                 $_ids = implode(',',$ids);



                 $query = "DELETE FROM `miniproject`.`phonebooks` WHERE `phonebooks`.`id` IN($_ids) ";

                 //Utility::dd( $query);
                 $result = mysql_query($query);

                 if($result){
                     Utility::message("Information are deleted successfully.");
                 }else{
                     Utility::message(" Cannot delete.");
                 }

                 Utility::redirect('index.php');
             }else{
                 Utility::message('No id avaiable. Sorry !');
                 return Utility::redirect('index.php');
             }
    }
    
       public function trash($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
       $this->id = $id;
       $this->title = $title;
      $this->operator = $operator;
       $this->mobile = $mobile;
        $this->telephone = $telephone;
       $this->description = $description;      
        $this->deleted_at = time();
        


        $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = '".$this->deleted_at. "' WHERE `phonebooks`.`id` = ".$this->id;

       // Utility::dd( $query);
        $result = mysql_query($query);
               
        if($result){
            Utility::message("information trashed successfully.");
        }else{
            Utility::message(" Cannot trash.");
        }
        
        Utility::redirect('index.php');
    }
        public function recovermultiple($ids = array()){
       
        if(is_array($ids) && count($ids) > 0){
           
            //Utility::dd($ids);
            $_ids = implode(',',$ids);



            $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = NULL WHERE `phonebooks`.`id` IN($_ids) ";

            //Utility::dd( $query);
            $result = mysql_query($query);

            if($result){
                Utility::message("Information is recovered successfully.");
            }else{
                Utility::message(" Cannot recover.");
            }

            Utility::redirect('index.php');
        }else{
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        

    }
    
    public function recover($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $this->id = $id;
        


        $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = NULL WHERE `phonebooks`.`id` = ".$this->id;

        //Utility::dd( $query);
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Information is recovered successfully.");
        }else{
            Utility::message(" Cannot recover.");
        }
        
        Utility::redirect('index.php');
    }
    
        public function update($id = null){
            
//          $query = "SELECT * FROM `miniproject`.`phonebooks` WHERE `phonebooks`.`id` = " . $id;
//          $result = mysql_query($query);
//          $row = mysql_fetch_assoc($result);
//          return $row;
            
            
           $query = "UPDATE `miniproject`.`phonebooks` SET `title` = '".$this->title."', `operator` = '".$this->operator."', `mobile` = '".$this->mobile."', `telephone` = '".$this->telephone."', `description` = '".$this->description."' WHERE `phonebooks`.`id` = ".$this->id;
          
         //  var_dump($query) or die();
           $result = mysql_query($query);
              
        if($result){
            Utility::message("Information is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    
}
?>